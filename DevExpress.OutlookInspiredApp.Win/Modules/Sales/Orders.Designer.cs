﻿namespace DevExpress.DevAV.Modules {
    partial class Orders {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleDataBar formatConditionRuleDataBar1 = new DevExpress.XtraEditors.FormatConditionRuleDataBar();
            DevExpress.XtraBars.Ribbon.GalleryItemGroup galleryItemGroup1 = new DevExpress.XtraBars.Ribbon.GalleryItemGroup();
            DevExpress.XtraBars.Ribbon.GalleryItem galleryItem1 = new DevExpress.XtraBars.Ribbon.GalleryItem();
            DevExpress.XtraBars.Ribbon.GalleryItem galleryItem2 = new DevExpress.XtraBars.Ribbon.GalleryItem();
            DevExpress.XtraBars.Ribbon.GalleryItem galleryItem3 = new DevExpress.XtraBars.Ribbon.GalleryItem();
            DevExpress.XtraBars.Ribbon.GalleryItem galleryItem4 = new DevExpress.XtraBars.Ribbon.GalleryItem();
            DevExpress.Skins.SkinPaddingEdges skinPaddingEdges1 = new DevExpress.Skins.SkinPaddingEdges();
            DevExpress.Skins.SkinPaddingEdges skinPaddingEdges2 = new DevExpress.Skins.SkinPaddingEdges();
            this.gridViewOrderItems = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colProduct = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProductUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProductPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDiscount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl = new DevExpress.XtraGrid.GridControl();
            this.bindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colInvoiceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrderDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStore = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCustomer = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShipDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShippingAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.modueLayout = new DevExpress.XtraLayout.LayoutControl();
            this.pnlView = new DevExpress.XtraEditors.PanelControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.masterItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.detailItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem = new DevExpress.XtraLayout.SplitterItem();
            this.ribbonControl = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.biNewOrder = new DevExpress.XtraBars.BarButtonItem();
            this.biNewGroup = new DevExpress.XtraBars.BarButtonItem();
            this.biDelete = new DevExpress.XtraBars.BarButtonItem();
            this.biShowList = new DevExpress.XtraBars.BarCheckItem();
            this.biMap = new DevExpress.XtraBars.BarButtonItem();
            this.biNewSubItem = new DevExpress.XtraBars.BarSubItem();
            this.bmiNewOrder = new DevExpress.XtraBars.BarButtonItem();
            this.bmiNewGroup = new DevExpress.XtraBars.BarButtonItem();
            this.biChangeViewSubItem = new DevExpress.XtraBars.BarSubItem();
            this.bmiShowList = new DevExpress.XtraBars.BarCheckItem();
            this.biDataPaneSubItem = new DevExpress.XtraBars.BarSubItem();
            this.bmiHorizontalLayout = new DevExpress.XtraBars.BarCheckItem();
            this.bmiVerticalLayout = new DevExpress.XtraBars.BarCheckItem();
            this.bmiHideDetail = new DevExpress.XtraBars.BarCheckItem();
            this.biResetView = new DevExpress.XtraBars.BarButtonItem();
            this.biPrintSubItem = new DevExpress.XtraBars.BarSubItem();
            this.bmiPrintInvoice = new DevExpress.XtraBars.BarButtonItem();
            this.bmiPrintSalesSummary = new DevExpress.XtraBars.BarButtonItem();
            this.bmiPrintSalesAnalysis = new DevExpress.XtraBars.BarButtonItem();
            this.biEdit = new DevExpress.XtraBars.BarButtonItem();
            this.galleryQuickReports = new DevExpress.XtraBars.RibbonGalleryBarItem();
            this.biViewSettings = new DevExpress.XtraBars.BarButtonItem();
            this.biReverseSort = new DevExpress.XtraBars.BarButtonItem();
            this.biAddColumns = new DevExpress.XtraBars.BarCheckItem();
            this.biExpandCollapse = new DevExpress.XtraBars.BarButtonItem();
            this.biNewCustomFilter = new DevExpress.XtraBars.BarButtonItem();
            this.hiItemsCount = new DevExpress.XtraBars.BarHeaderItem();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup8 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup4 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup6 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage2 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup5 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup7 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup9 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonStatusBar = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            ((System.ComponentModel.ISupportInitialize)(this.mvvmContext)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewOrderItems)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.modueLayout)).BeginInit();
            this.modueLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.masterItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detailItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl)).BeginInit();
            this.SuspendLayout();
            this.gridViewOrderItems.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colProduct,
            this.colProductUnits,
            this.colProductPrice,
            this.colDiscount,
            this.colTotal,
            this.colId});
            this.gridViewOrderItems.GridControl = this.gridControl;
            this.gridViewOrderItems.Name = "gridViewOrderItems";
            this.gridViewOrderItems.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewOrderItems.OptionsView.ShowFooter = true;
            this.gridViewOrderItems.OptionsView.ShowGroupPanel = false;
            this.gridViewOrderItems.OptionsView.ShowIndicator = false;
            this.gridViewOrderItems.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.False;
            this.colProduct.Caption = "<b>PRODUCT</b>";
            this.colProduct.FieldName = "Product.Name";
            this.colProduct.Name = "colProduct";
            this.colProduct.OptionsColumn.AllowFocus = false;
            this.colProduct.Visible = true;
            this.colProduct.VisibleIndex = 0;
            this.colProductUnits.Caption = "UNITS";
            this.colProductUnits.FieldName = "ProductUnits";
            this.colProductUnits.Name = "colProductUnits";
            this.colProductUnits.OptionsColumn.AllowFocus = false;
            this.colProductUnits.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ProductUnits", "SUM={0}")});
            this.colProductUnits.Visible = true;
            this.colProductUnits.VisibleIndex = 1;
            this.colProductPrice.Caption = "UNIT PRICE";
            this.colProductPrice.DisplayFormat.FormatString = "c";
            this.colProductPrice.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colProductPrice.FieldName = "ProductPrice";
            this.colProductPrice.Name = "colProductPrice";
            this.colProductPrice.OptionsColumn.AllowFocus = false;
            this.colProductPrice.Visible = true;
            this.colProductPrice.VisibleIndex = 2;
            this.colDiscount.Caption = "DISCOUNT";
            this.colDiscount.DisplayFormat.FormatString = "c";
            this.colDiscount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colDiscount.FieldName = "Discount";
            this.colDiscount.Name = "colDiscount";
            this.colDiscount.OptionsColumn.AllowFocus = false;
            this.colDiscount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Average, "Discount", "AVG={0:c}")});
            this.colDiscount.Visible = true;
            this.colDiscount.VisibleIndex = 3;
            this.colTotal.Caption = "TOTAL";
            this.colTotal.DisplayFormat.FormatString = "c";
            this.colTotal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTotal.FieldName = "Total";
            this.colTotal.Name = "colTotal";
            this.colTotal.OptionsColumn.AllowFocus = false;
            this.colTotal.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Total", "SUM={0:c}")});
            this.colTotal.Visible = true;
            this.colTotal.VisibleIndex = 4;
            this.colId.FieldName = "Id";
            this.colId.Name = "colId";
            this.colId.OptionsColumn.AllowFocus = false;
            this.gridControl.DataSource = this.bindingSource;
            gridLevelNode1.LevelTemplate = this.gridViewOrderItems;
            gridLevelNode1.RelationName = "OrderItems";
            this.gridControl.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl.Location = new System.Drawing.Point(4, 12);
            this.gridControl.MainView = this.gridView;
            this.gridControl.Margin = new System.Windows.Forms.Padding(12);
            this.gridControl.Name = "gridControl";
            this.gridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoEdit1});
            this.gridControl.ShowOnlyPredefinedDetails = true;
            this.gridControl.Size = new System.Drawing.Size(782, 539);
            this.gridControl.TabIndex = 1;
            this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView,
            this.gridViewOrderItems});
            this.bindingSource.DataSource = typeof(DevExpress.DevAV.Order);
            this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colInvoiceNumber,
            this.colOrderDate,
            this.colTotalAmount,
            this.colStore,
            this.colCustomer,
            this.colShipDate1,
            this.colShippingAmount});
            this.gridView.DetailVerticalIndent = 20;
            gridFormatRule1.Column = this.colTotalAmount;
            gridFormatRule1.Name = "Format0";
            formatConditionRuleDataBar1.PredefinedName = "Mint Gradient";
            gridFormatRule1.Rule = formatConditionRuleDataBar1;
            this.gridView.FormatRules.Add(gridFormatRule1);
            this.gridView.GridControl = this.gridControl;
            this.gridView.Name = "gridView";
            this.gridView.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView.OptionsBehavior.Editable = false;
            this.gridView.OptionsBehavior.ReadOnly = true;
            this.gridView.OptionsDetail.AllowZoomDetail = false;
            this.gridView.OptionsDetail.DetailMode = DevExpress.XtraGrid.Views.Grid.DetailMode.Embedded;
            this.gridView.OptionsDetail.ShowDetailTabs = false;
            this.gridView.OptionsDetail.SmartDetailExpandButtonMode = DevExpress.XtraGrid.Views.Grid.DetailExpandButtonMode.AlwaysEnabled;
            this.gridView.OptionsDetail.SmartDetailHeight = true;
            this.gridView.OptionsFind.AlwaysVisible = true;
            this.gridView.OptionsFind.FindNullPrompt = "Search Orders (Ctrl + F)";
            this.gridView.OptionsFind.ShowClearButton = false;
            this.gridView.OptionsFind.ShowFindButton = false;
            this.gridView.OptionsView.ShowGroupPanel = false;
            this.gridView.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView.OptionsBehavior.AllowPixelScrolling = Utils.DefaultBoolean.False;
            this.gridView.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrderDate, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.colInvoiceNumber.Caption = "INVOICE #";
            this.colInvoiceNumber.FieldName = "InvoiceNumber";
            this.colInvoiceNumber.Name = "colInvoiceNumber";
            this.colInvoiceNumber.OptionsColumn.AllowFocus = false;
            this.colInvoiceNumber.Visible = true;
            this.colInvoiceNumber.VisibleIndex = 0;
            this.colInvoiceNumber.Width = 106;
            this.colOrderDate.Caption = "ORDER DATE";
            this.colOrderDate.FieldName = "OrderDate";
            this.colOrderDate.Name = "colOrderDate";
            this.colOrderDate.OptionsColumn.AllowFocus = false;
            this.colOrderDate.Visible = true;
            this.colOrderDate.VisibleIndex = 1;
            this.colOrderDate.Width = 133;
            this.colTotalAmount.Caption = "ORDER TOTAL";
            this.colTotalAmount.DisplayFormat.FormatString = "c";
            this.colTotalAmount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTotalAmount.FieldName = "TotalAmount";
            this.colTotalAmount.Name = "colTotalAmount";
            this.colTotalAmount.OptionsColumn.AllowFocus = false;
            this.colTotalAmount.Visible = true;
            this.colTotalAmount.VisibleIndex = 4;
            this.colTotalAmount.Width = 149;
            this.colStore.Caption = "STORE";
            this.colStore.FieldName = "Store.CrestCity";
            this.colStore.Name = "colStore";
            this.colStore.OptionsColumn.AllowFocus = false;
            this.colStore.Visible = true;
            this.colStore.VisibleIndex = 3;
            this.colStore.Width = 180;
            this.colCustomer.Caption = "COMPANY";
            this.colCustomer.FieldName = "Customer.Name";
            this.colCustomer.Name = "colCustomer";
            this.colCustomer.OptionsColumn.AllowFocus = false;
            this.colCustomer.Visible = true;
            this.colCustomer.VisibleIndex = 2;
            this.colCustomer.Width = 191;
            this.colShipDate1.Caption = "SHIP DATE";
            this.colShipDate1.FieldName = "ShipDate";
            this.colShipDate1.Name = "colShipDate1";
            this.colShipDate1.OptionsColumn.AllowFocus = false;
            this.colShippingAmount.Caption = "SHIPPING AMOUNT";
            this.colShippingAmount.DisplayFormat.FormatString = "c";
            this.colShippingAmount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colShippingAmount.FieldName = "ShippingAmount";
            this.colShippingAmount.Name = "colShippingAmount";
            this.colShippingAmount.OptionsColumn.AllowFocus = false;
            this.repositoryItemMemoEdit1.Name = "repositoryItemMemoEdit1";
            this.modueLayout.AllowCustomization = false;
            this.modueLayout.Controls.Add(this.pnlView);
            this.modueLayout.Controls.Add(this.gridControl);
            this.modueLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.modueLayout.Location = new System.Drawing.Point(0, 141);
            this.modueLayout.Name = "modueLayout";
            this.modueLayout.Root = this.layoutControlGroup1;
            this.modueLayout.Size = new System.Drawing.Size(1273, 563);
            this.modueLayout.TabIndex = 5;
            this.modueLayout.Text = "modueLayout";
            this.pnlView.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlView.Location = new System.Drawing.Point(795, 12);
            this.pnlView.Name = "pnlView";
            this.pnlView.Size = new System.Drawing.Size(466, 539);
            this.pnlView.TabIndex = 4;
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.masterItem,
            this.detailItem,
            this.splitterItem});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 10, 10, 10);
            this.layoutControlGroup1.Size = new System.Drawing.Size(1273, 563);
            this.layoutControlGroup1.TextVisible = false;
            this.masterItem.Control = this.gridControl;
            this.masterItem.CustomizationFormText = "masterItem";
            this.masterItem.Location = new System.Drawing.Point(0, 0);
            this.masterItem.Name = "masterItem";
            this.masterItem.Size = new System.Drawing.Size(786, 543);
            this.masterItem.TextSize = new System.Drawing.Size(0, 0);
            this.masterItem.TextVisible = false;
            this.detailItem.Control = this.pnlView;
            this.detailItem.CustomizationFormText = "detailItem";
            this.detailItem.Location = new System.Drawing.Point(791, 0);
            this.detailItem.Name = "detailItem";
            this.detailItem.Size = new System.Drawing.Size(470, 543);
            this.detailItem.TextSize = new System.Drawing.Size(0, 0);
            this.detailItem.TextVisible = false;
            this.splitterItem.AllowHotTrack = true;
            this.splitterItem.CustomizationFormText = "splitterItem1";
            this.splitterItem.Location = new System.Drawing.Point(786, 0);
            this.splitterItem.Name = "splitterItem1";
            this.splitterItem.Size = new System.Drawing.Size(5, 543);
            this.ribbonControl.ExpandCollapseItem.Id = 0;
            this.ribbonControl.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl.ExpandCollapseItem,
            this.biNewOrder,
            this.biNewGroup,
            this.biDelete,
            this.biShowList,
            this.biMap,
            this.biNewSubItem,
            this.biChangeViewSubItem,
            this.biDataPaneSubItem,
            this.bmiHideDetail,
            this.bmiHorizontalLayout,
            this.bmiVerticalLayout,
            this.biResetView,
            this.bmiShowList,
            this.bmiNewOrder,
            this.bmiNewGroup,
            this.biPrintSubItem,
            this.bmiPrintInvoice,
            this.biEdit,
            this.galleryQuickReports,
            this.biViewSettings,
            this.biReverseSort,
            this.biAddColumns,
            this.biExpandCollapse,
            this.biNewCustomFilter,
            this.bmiPrintSalesSummary,
            this.bmiPrintSalesAnalysis,
            this.hiItemsCount});
            this.ribbonControl.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl.MaxItemId = 11;
            this.ribbonControl.Name = "ribbonControl";
            this.ribbonControl.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1,
            this.ribbonPage2});
            this.ribbonControl.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.Office2013;
            this.ribbonControl.Size = new System.Drawing.Size(1273, 141);
            this.ribbonControl.StatusBar = this.ribbonStatusBar;
            this.biNewOrder.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.False;
            this.biNewOrder.Caption = "New Order";
            this.biNewOrder.Id = 1;
            this.biNewOrder.ImageUri.ResourceType = typeof(DevExpress.DevAV.MainForm);
            this.biNewOrder.ImageUri.Uri = "resource://DevExpress.DevAV.Resources.NewOrder.svg";
            this.biNewOrder.Name = "biNewOrder";
            this.biNewGroup.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.False;
            this.biNewGroup.Caption = "New Group";
            this.biNewGroup.Id = 2;
            this.biNewGroup.ImageUri.ResourceType = typeof(DevExpress.DevAV.MainForm);
            this.biNewGroup.ImageUri.Uri = "resource://DevExpress.DevAV.Resources.NewGroup.svg";
            this.biNewGroup.Name = "biNewGroup";
            this.biDelete.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.False;
            this.biDelete.Caption = "Delete";
            this.biDelete.Id = 3;
            this.biDelete.ImageUri.ResourceType = typeof(DevExpress.DevAV.MainForm);
            this.biDelete.ImageUri.Uri = "resource://DevExpress.DevAV.Resources.Delete.svg";
            this.biDelete.Name = "biDelete";
            this.biShowList.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.False;
            this.biShowList.Caption = "List";
            this.biShowList.GroupIndex = 1;
            this.biShowList.Id = 4;
            this.biShowList.ImageUri.ResourceType = typeof(DevExpress.DevAV.MainForm);
            this.biShowList.ImageUri.Uri = "resource://DevExpress.DevAV.Resources.List.svg";
            this.biShowList.LargeImageIndex = 37;
            this.biShowList.Name = "biShowList";
            this.biMap.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.False;
            this.biMap.Caption = "Shipping Map";
            this.biMap.Id = 8;
            this.biMap.ImageUri.ResourceType = typeof(DevExpress.DevAV.MainForm);
            this.biMap.ImageUri.Uri = "resource://DevExpress.DevAV.Resources.Mapit.svg";
            this.biMap.Name = "biMap";
            this.biNewSubItem.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.False;
            this.biNewSubItem.Caption = "New Items";
            this.biNewSubItem.Id = 10;
            this.biNewSubItem.ImageUri.ResourceType = typeof(DevExpress.DevAV.MainForm);
            this.biNewSubItem.ImageUri.Uri = "resource://DevExpress.DevAV.Resources.NewItem.svg";
            this.biNewSubItem.LargeImageIndex = 29;
            this.biNewSubItem.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bmiNewOrder),
            new DevExpress.XtraBars.LinkPersistInfo(this.bmiNewGroup)});
            this.biNewSubItem.Name = "biNewSubItem";
            this.biNewSubItem.ShowNavigationHeader = DevExpress.Utils.DefaultBoolean.True;
            this.bmiNewOrder.Caption = "New Order";
            this.bmiNewOrder.Glyph = global::DevExpress.DevAV.Properties.Resources.icon_new_sales_16;
            this.bmiNewOrder.Id = 11;
            this.bmiNewOrder.Name = "bmiNewOrder";
            this.bmiNewGroup.Caption = "New Group";
            this.bmiNewGroup.Glyph = global::DevExpress.DevAV.Properties.Resources.icon_new_group_16;
            this.bmiNewGroup.Id = 12;
            this.bmiNewGroup.Name = "bmiNewGroup";
            this.biChangeViewSubItem.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.False;
            this.biChangeViewSubItem.Caption = "Change View";
            this.biChangeViewSubItem.Id = 10;
            this.biChangeViewSubItem.ImageUri.ResourceType = typeof(DevExpress.DevAV.MainForm);
            this.biChangeViewSubItem.ImageUri.Uri = "resource://DevExpress.DevAV.Resources.ChangeView.svg";
            this.biChangeViewSubItem.LargeImageIndex = 40;
            this.biChangeViewSubItem.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bmiShowList)});
            this.biChangeViewSubItem.Name = "biChangeViewSubItem";
            this.biChangeViewSubItem.ShowNavigationHeader = DevExpress.Utils.DefaultBoolean.True;
            this.bmiShowList.Caption = "List";
            this.bmiShowList.Glyph = global::DevExpress.DevAV.Properties.Resources.icon_list_32;
            this.bmiShowList.GroupIndex = 3;
            this.bmiShowList.Id = 4;
            this.bmiShowList.Name = "bmiShowList";
            this.biDataPaneSubItem.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.False;
            this.biDataPaneSubItem.Caption = "Data Pane";
            this.biDataPaneSubItem.Id = 10;
            this.biDataPaneSubItem.ImageUri.ResourceType = typeof(DevExpress.DevAV.MainForm);
            this.biDataPaneSubItem.ImageUri.Uri = "resource://DevExpress.DevAV.Resources.DataPanel.svg";
            this.biDataPaneSubItem.LargeImageIndex = 41;
            this.biDataPaneSubItem.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bmiHorizontalLayout),
            new DevExpress.XtraBars.LinkPersistInfo(this.bmiVerticalLayout),
            new DevExpress.XtraBars.LinkPersistInfo(this.bmiHideDetail)});
            this.biDataPaneSubItem.Name = "biDataPaneSubItem";
            this.biDataPaneSubItem.ShowNavigationHeader = DevExpress.Utils.DefaultBoolean.True;
            this.bmiHorizontalLayout.Caption = "Right";
            this.bmiHorizontalLayout.GroupIndex = 2;
            this.bmiHorizontalLayout.Id = 6;
            this.bmiHorizontalLayout.ImageUri.ResourceType = typeof(DevExpress.DevAV.MainForm);
            this.bmiHorizontalLayout.ImageUri.Uri = "resource://DevExpress.DevAV.Resources.DataPanel.svg?Size=16x16";
            this.bmiHorizontalLayout.Name = "bmiHorizontalLayout";
            this.bmiVerticalLayout.Caption = "Bottom";
            this.bmiVerticalLayout.GroupIndex = 2;
            this.bmiVerticalLayout.Id = 7;
            this.bmiVerticalLayout.ImageUri.ResourceType = typeof(DevExpress.DevAV.MainForm);
            this.bmiVerticalLayout.ImageUri.Uri = "resource://DevExpress.DevAV.Resources.PanelBottom.svg?Size=16x16";
            this.bmiVerticalLayout.Name = "bmiVerticalLayout";
            this.bmiHideDetail.Caption = "Off";
            this.bmiHideDetail.GroupIndex = 2;
            this.bmiHideDetail.Id = 7;
            this.bmiHideDetail.ImageUri.ResourceType = typeof(DevExpress.DevAV.MainForm);
            this.bmiHideDetail.ImageUri.Uri = "resource://DevExpress.DevAV.Resources.PanelOff.svg?Size=16x16";
            this.bmiHideDetail.Name = "bmiHideDetail";
            this.biResetView.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.False;
            this.biResetView.Caption = "Reset View";
            this.biResetView.Id = 9;
            this.biResetView.ImageUri.ResourceType = typeof(DevExpress.DevAV.MainForm);
            this.biResetView.ImageUri.Uri = "resource://DevExpress.DevAV.Resources.ResetView.svg";
            this.biResetView.Name = "biResetView";
            this.biPrintSubItem.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.False;
            this.biPrintSubItem.Caption = "Print";
            this.biPrintSubItem.Id = 10;
            this.biPrintSubItem.ImageUri.ResourceType = typeof(DevExpress.DevAV.MainForm);
            this.biPrintSubItem.ImageUri.Uri = "resource://DevExpress.DevAV.Resources.Print.svg";
            this.biPrintSubItem.LargeImageIndex = 30;
            this.biPrintSubItem.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bmiPrintInvoice),
            new DevExpress.XtraBars.LinkPersistInfo(this.bmiPrintSalesSummary),
            new DevExpress.XtraBars.LinkPersistInfo(this.bmiPrintSalesAnalysis)});
            this.biPrintSubItem.Name = "biPrintSubItem";
            this.biPrintSubItem.ShowNavigationHeader = DevExpress.Utils.DefaultBoolean.True;
            this.bmiPrintInvoice.Caption = "Invoice";
            this.bmiPrintInvoice.Glyph = global::DevExpress.DevAV.Properties.Resources.icon_sales_invoice_16;
            this.bmiPrintInvoice.GroupIndex = 5;
            this.bmiPrintInvoice.Id = 6;
            this.bmiPrintInvoice.Name = "bmiPrintInvoice";
            this.bmiPrintSalesSummary.Caption = "Summary Report";
            this.bmiPrintSalesSummary.Glyph = global::DevExpress.DevAV.Properties.Resources.icon_sales_report_16;
            this.bmiPrintSalesSummary.Id = 9;
            this.bmiPrintSalesSummary.Name = "bmiPrintSalesSummary";
            this.bmiPrintSalesAnalysis.Caption = "Sales Analisys";
            this.bmiPrintSalesAnalysis.Glyph = global::DevExpress.DevAV.Properties.Resources.icon_sales_by_store_16;
            this.bmiPrintSalesAnalysis.Id = 10;
            this.bmiPrintSalesAnalysis.Name = "bmiPrintSalesAnalysis";
            this.biEdit.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.False;
            this.biEdit.Caption = "Edit";
            this.biEdit.Id = 13;
            this.biEdit.ImageUri.ResourceType = typeof(DevExpress.DevAV.MainForm);
            this.biEdit.ImageUri.Uri = "resource://DevExpress.DevAV.Resources.Edit.svg";
            this.biEdit.Name = "biEdit";
            this.galleryQuickReports.Caption = "ribbonGalleryBarItem1";
            this.galleryQuickReports.Gallery.ColumnCount = 2;
            this.galleryQuickReports.Gallery.DrawImageBackground = false;
            galleryItemGroup1.Caption = "Group1";
            galleryItem1.Caption = "Sales Report";
            galleryItem1.Image = global::DevExpress.DevAV.Properties.Resources.icon_sales_quick_report_16;
            galleryItem2.Caption = "Thank You";
            galleryItem2.Image = global::DevExpress.DevAV.Properties.Resources.icon_sales_quick_thankyou_16;
            galleryItem3.Caption = "Sales by Store";
            galleryItem3.Image = global::DevExpress.DevAV.Properties.Resources.icon_sales_quick_summary_16;
            galleryItem4.Caption = "Invoice";
            galleryItem4.Image = global::DevExpress.DevAV.Properties.Resources.icon_sales_quick_invoice_16;
            galleryItemGroup1.Items.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItem[] {
            galleryItem1,
            galleryItem2,
            galleryItem3,
            galleryItem4});
            this.galleryQuickReports.Gallery.Groups.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItemGroup[] {
            galleryItemGroup1});
            this.galleryQuickReports.Gallery.ItemImageLocation = DevExpress.Utils.Locations.Left;
            skinPaddingEdges1.Bottom = -3;
            skinPaddingEdges1.Top = -3;
            this.galleryQuickReports.Gallery.ItemImagePadding = skinPaddingEdges1;
            skinPaddingEdges2.Bottom = -1;
            skinPaddingEdges2.Top = -1;
            this.galleryQuickReports.Gallery.ItemTextPadding = skinPaddingEdges2;
            this.galleryQuickReports.Gallery.ShowItemText = true;
            this.galleryQuickReports.Id = 15;
            this.galleryQuickReports.Name = "galleryQuickReports";
            this.biViewSettings.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.False;
            this.biViewSettings.Caption = "View Settings";
            this.biViewSettings.Id = 3;
            this.biViewSettings.ImageUri.ResourceType = typeof(DevExpress.DevAV.MainForm);
            this.biViewSettings.ImageUri.Uri = "resource://DevExpress.DevAV.Resources.ViewSetting.svg";
            this.biViewSettings.Name = "biViewSettings";
            this.biReverseSort.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.False;
            this.biReverseSort.Caption = "Reverse Sort";
            this.biReverseSort.Id = 4;
            this.biReverseSort.ImageUri.ResourceType = typeof(DevExpress.DevAV.MainForm);
            this.biReverseSort.ImageUri.Uri = "resource://DevExpress.DevAV.Resources.ReversSort.svg?Size=16x16";
            this.biReverseSort.Name = "biReverseSort";
            this.biReverseSort.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.biReverseSort_ItemClick);
            this.biAddColumns.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.False;
            this.biAddColumns.Caption = "Add Columns";
            this.biAddColumns.Id = 5;
            this.biAddColumns.ImageUri.ResourceType = typeof(DevExpress.DevAV.MainForm);
            this.biAddColumns.ImageUri.Uri = "resource://DevExpress.DevAV.Resources.AddColumn.svg?Size=16x16";
            this.biAddColumns.Name = "biAddColumns";
            this.biAddColumns.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.biAddColumns_ItemCheckedChanged);
            this.biExpandCollapse.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.False;
            this.biExpandCollapse.Caption = "Expand/Collapse";
            this.biExpandCollapse.Id = 6;
            this.biExpandCollapse.ImageUri.ResourceType = typeof(DevExpress.DevAV.MainForm);
            this.biExpandCollapse.ImageUri.Uri = "resource://DevExpress.DevAV.Resources.ExpandCollapse.svg?Size=16x16";
            this.biExpandCollapse.Name = "biExpandCollapse";
            this.biExpandCollapse.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.biExpandCollapse_ItemClick);
            this.biNewCustomFilter.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.False;
            this.biNewCustomFilter.Caption = "Custom Filter";
            this.biNewCustomFilter.Id = 8;
            this.biNewCustomFilter.ImageUri.ResourceType = typeof(DevExpress.DevAV.MainForm);
            this.biNewCustomFilter.ImageUri.Uri = "resource://DevExpress.DevAV.Resources.Filter.svg";
            this.biNewCustomFilter.Name = "biNewCustomFilter";
            this.hiItemsCount.Caption = "RECORDS: 0";
            this.hiItemsCount.Id = 7;
            this.hiItemsCount.Name = "hiItemsCount";
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1,
            this.ribbonPageGroup2,
            this.ribbonPageGroup3,
            this.ribbonPageGroup8,
            this.ribbonPageGroup4,
            this.ribbonPageGroup6});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "HOME";
            this.ribbonPageGroup1.AllowTextClipping = false;
            this.ribbonPageGroup1.ItemLinks.Add(this.biNewOrder);
            this.ribbonPageGroup1.ItemLinks.Add(this.biNewGroup);
            this.ribbonPageGroup1.ItemLinks.Add(this.biNewSubItem);
            this.ribbonPageGroup1.MergeOrder = 0;
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.ShowCaptionButton = false;
            this.ribbonPageGroup1.Text = "New";
            this.ribbonPageGroup2.AllowTextClipping = false;
            this.ribbonPageGroup2.ItemLinks.Add(this.biDelete);
            this.ribbonPageGroup2.MergeOrder = 0;
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            this.ribbonPageGroup2.ShowCaptionButton = false;
            this.ribbonPageGroup2.Text = "Delete";
            this.ribbonPageGroup3.AllowTextClipping = false;
            this.ribbonPageGroup3.ItemLinks.Add(this.biEdit);
            this.ribbonPageGroup3.ItemLinks.Add(this.biPrintSubItem);
            this.ribbonPageGroup3.MergeOrder = 0;
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            this.ribbonPageGroup3.ShowCaptionButton = false;
            this.ribbonPageGroup3.Text = "Actions";
            this.ribbonPageGroup8.AllowTextClipping = false;
            this.ribbonPageGroup8.ItemLinks.Add(this.galleryQuickReports);
            this.ribbonPageGroup8.MergeOrder = 0;
            this.ribbonPageGroup8.Name = "ribbonPageGroup8";
            this.ribbonPageGroup8.ShowCaptionButton = false;
            this.ribbonPageGroup8.Text = "Quick Reports";
            this.ribbonPageGroup4.AllowTextClipping = false;
            this.ribbonPageGroup4.ItemLinks.Add(this.biShowList);
            this.ribbonPageGroup4.ItemLinks.Add(this.biMap);
            this.ribbonPageGroup4.MergeOrder = 0;
            this.ribbonPageGroup4.Name = "ribbonPageGroup4";
            this.ribbonPageGroup4.ShowCaptionButton = false;
            this.ribbonPageGroup4.Text = "View";
            this.ribbonPageGroup6.AllowTextClipping = false;
            this.ribbonPageGroup6.ItemLinks.Add(this.biNewCustomFilter);
            this.ribbonPageGroup6.MergeOrder = 0;
            this.ribbonPageGroup6.Name = "ribbonPageGroup6";
            this.ribbonPageGroup6.ShowCaptionButton = false;
            this.ribbonPageGroup6.Text = "Find";
            this.ribbonPage2.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup5,
            this.ribbonPageGroup7,
            this.ribbonPageGroup9});
            this.ribbonPage2.Name = "ribbonPage2";
            this.ribbonPage2.Text = "VIEW";
            this.ribbonPageGroup5.AllowTextClipping = false;
            this.ribbonPageGroup5.ItemLinks.Add(this.biChangeViewSubItem);
            this.ribbonPageGroup5.ItemLinks.Add(this.biViewSettings);
            this.ribbonPageGroup5.ItemLinks.Add(this.biResetView);
            this.ribbonPageGroup5.MergeOrder = 0;
            this.ribbonPageGroup5.Name = "ribbonPageGroup5";
            this.ribbonPageGroup5.ShowCaptionButton = false;
            this.ribbonPageGroup5.Text = "Current View";
            this.ribbonPageGroup7.AllowTextClipping = false;
            this.ribbonPageGroup7.ItemLinks.Add(this.biDataPaneSubItem);
            this.ribbonPageGroup7.MergeOrder = 1;
            this.ribbonPageGroup7.Name = "ribbonPageGroup7";
            this.ribbonPageGroup7.ShowCaptionButton = false;
            this.ribbonPageGroup7.Text = "Layout";
            this.ribbonPageGroup9.AllowTextClipping = false;
            this.ribbonPageGroup9.ItemLinks.Add(this.biReverseSort);
            this.ribbonPageGroup9.ItemLinks.Add(this.biAddColumns);
            this.ribbonPageGroup9.ItemLinks.Add(this.biExpandCollapse);
            this.ribbonPageGroup9.MergeOrder = 1;
            this.ribbonPageGroup9.Name = "ribbonPageGroup9";
            this.ribbonStatusBar.ItemLinks.Add(this.hiItemsCount);
            this.ribbonStatusBar.Location = new System.Drawing.Point(0, 704);
            this.ribbonStatusBar.Name = "ribbonStatusBar";
            this.ribbonStatusBar.Ribbon = this.ribbonControl;
            this.ribbonStatusBar.Size = new System.Drawing.Size(1273, 27);
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.modueLayout);
            this.Controls.Add(this.ribbonControl);
            this.Controls.Add(this.ribbonStatusBar);
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "Orders";
            this.Size = new System.Drawing.Size(1273, 731);
            ((System.ComponentModel.ISupportInitialize)(this.mvvmContext)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewOrderItems)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.modueLayout)).EndInit();
            this.modueLayout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.masterItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detailItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private XtraGrid.GridControl gridControl;
        private XtraGrid.Views.Grid.GridView gridView;
        private System.Windows.Forms.BindingSource bindingSource;
        private XtraLayout.LayoutControl modueLayout;
        private XtraLayout.LayoutControlGroup layoutControlGroup1;
        private XtraLayout.LayoutControlItem masterItem;
        private XtraEditors.PanelControl pnlView;
        private XtraLayout.LayoutControlItem detailItem;
        private XtraLayout.SplitterItem splitterItem;
        private XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit1;
        private XtraGrid.Columns.GridColumn colOrderDate;
        private XtraGrid.Columns.GridColumn colTotalAmount;
        private XtraGrid.Views.Grid.GridView gridViewOrderItems;
        private XtraGrid.Columns.GridColumn colProduct;
        private XtraGrid.Columns.GridColumn colProductUnits;
        private XtraGrid.Columns.GridColumn colProductPrice;
        private XtraGrid.Columns.GridColumn colDiscount;
        private XtraGrid.Columns.GridColumn colTotal;
        private XtraGrid.Columns.GridColumn colId;
        private XtraBars.Ribbon.RibbonControl ribbonControl;
        private XtraBars.BarButtonItem biNewOrder;
        private XtraBars.BarButtonItem biNewGroup;
        private XtraBars.BarButtonItem biDelete;
        private XtraBars.BarCheckItem biShowList;
        private XtraBars.BarButtonItem biMap;
        private XtraBars.BarSubItem biNewSubItem;
        private XtraBars.BarButtonItem bmiNewOrder;
        private XtraBars.BarButtonItem bmiNewGroup;
        private XtraBars.BarSubItem biChangeViewSubItem;
        private XtraBars.BarCheckItem bmiShowList;
        private XtraBars.BarSubItem biDataPaneSubItem;
        private XtraBars.BarCheckItem bmiHorizontalLayout;
        private XtraBars.BarCheckItem bmiVerticalLayout;
        private XtraBars.BarCheckItem bmiHideDetail;
        private XtraBars.BarButtonItem biResetView;
        private XtraBars.BarSubItem biPrintSubItem;
        private XtraBars.BarButtonItem bmiPrintInvoice;
        private XtraBars.BarButtonItem bmiPrintSalesSummary;
        private XtraBars.BarButtonItem bmiPrintSalesAnalysis;
        private XtraBars.BarButtonItem biEdit;
        private XtraBars.RibbonGalleryBarItem galleryQuickReports;
        private XtraBars.BarButtonItem biViewSettings;
        private XtraBars.BarButtonItem biReverseSort;
        private XtraBars.BarCheckItem biAddColumns;
        private XtraBars.BarButtonItem biExpandCollapse;
        private XtraBars.Ribbon.RibbonPage ribbonPage1;
        private XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
        private XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup8;
        private XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup4;
        private XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup6;
        private XtraBars.Ribbon.RibbonPage ribbonPage2;
        private XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup5;
        private XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup7;
        private XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup9;
        private XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar;
        private XtraBars.BarHeaderItem hiItemsCount;
        private XtraGrid.Columns.GridColumn colInvoiceNumber;
        private XtraGrid.Columns.GridColumn colStore;
        private XtraGrid.Columns.GridColumn colCustomer;
        private XtraGrid.Columns.GridColumn colShipDate1;
        private XtraGrid.Columns.GridColumn colShippingAmount;
        private XtraBars.BarButtonItem biNewCustomFilter;
    }
}
