﻿namespace DevExpress.DevAV.Modules {
    partial class EmployeeEditView {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraCharts.XYDiagram xyDiagram1 = new DevExpress.XtraCharts.XYDiagram();
            DevExpress.XtraCharts.Series series1 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.PointSeriesLabel pointSeriesLabel1 = new DevExpress.XtraCharts.PointSeriesLabel();
            DevExpress.XtraCharts.SeriesPoint seriesPoint1 = new DevExpress.XtraCharts.SeriesPoint(new System.DateTime(2017, 5, 1, 0, 0, 0, 0), new object[] {
            ((object)(1000000D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint2 = new DevExpress.XtraCharts.SeriesPoint(new System.DateTime(2017, 5, 2, 0, 0, 0, 0), new object[] {
            ((object)(2000000D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint3 = new DevExpress.XtraCharts.SeriesPoint(new System.DateTime(2017, 5, 3, 0, 0, 0, 0), new object[] {
            ((object)(2000000D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint4 = new DevExpress.XtraCharts.SeriesPoint(new System.DateTime(2017, 5, 4, 0, 0, 0, 0), new object[] {
            ((object)(3000000D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint5 = new DevExpress.XtraCharts.SeriesPoint(new System.DateTime(2017, 5, 5, 0, 0, 0, 0), new object[] {
            ((object)(2000000D))});
            DevExpress.XtraCharts.LineSeriesView lineSeriesView1 = new DevExpress.XtraCharts.LineSeriesView();
            DevExpress.XtraCharts.Series series2 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.PointSeriesLabel pointSeriesLabel2 = new DevExpress.XtraCharts.PointSeriesLabel();
            DevExpress.XtraCharts.SeriesPoint seriesPoint6 = new DevExpress.XtraCharts.SeriesPoint(new System.DateTime(2017, 5, 1, 0, 0, 0, 0), new object[] {
            ((object)(700000D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint7 = new DevExpress.XtraCharts.SeriesPoint(new System.DateTime(2017, 5, 2, 0, 0, 0, 0), new object[] {
            ((object)(900000D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint8 = new DevExpress.XtraCharts.SeriesPoint(new System.DateTime(2017, 5, 3, 0, 0, 0, 0), new object[] {
            ((object)(1200000D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint9 = new DevExpress.XtraCharts.SeriesPoint(new System.DateTime(2017, 5, 4, 0, 0, 0, 0), new object[] {
            ((object)(1000000D))});
            DevExpress.XtraCharts.SeriesPoint seriesPoint10 = new DevExpress.XtraCharts.SeriesPoint(new System.DateTime(2017, 5, 5, 0, 0, 0, 0), new object[] {
            ((object)(400000D))});
            DevExpress.XtraCharts.LineSeriesView lineSeriesView2 = new DevExpress.XtraCharts.LineSeriesView();
            DevExpress.XtraBars.Ribbon.GalleryItemGroup galleryItemGroup1 = new DevExpress.XtraBars.Ribbon.GalleryItemGroup();
            DevExpress.XtraBars.Ribbon.GalleryItem galleryItem1 = new DevExpress.XtraBars.Ribbon.GalleryItem();
            DevExpress.XtraBars.Ribbon.GalleryItem galleryItem2 = new DevExpress.XtraBars.Ribbon.GalleryItem();
            DevExpress.XtraBars.Ribbon.GalleryItem galleryItem3 = new DevExpress.XtraBars.Ribbon.GalleryItem();
            DevExpress.XtraBars.Ribbon.GalleryItem galleryItem4 = new DevExpress.XtraBars.Ribbon.GalleryItem();
            DevExpress.XtraBars.Ribbon.GalleryItem galleryItem5 = new DevExpress.XtraBars.Ribbon.GalleryItem();
            DevExpress.Skins.SkinPaddingEdges skinPaddingEdges1 = new DevExpress.Skins.SkinPaddingEdges();
            DevExpress.Skins.SkinPaddingEdges skinPaddingEdges2 = new DevExpress.Skins.SkinPaddingEdges();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            this.moduleDataLayout = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.chartControl1 = new DevExpress.XtraCharts.ChartControl();
            this.FullNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.bindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ribbonControl = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.biSave = new DevExpress.XtraBars.BarButtonItem();
            this.biClose = new DevExpress.XtraBars.BarButtonItem();
            this.biSaveAndClose = new DevExpress.XtraBars.BarButtonItem();
            this.biDelete = new DevExpress.XtraBars.BarButtonItem();
            this.biMailMerge = new DevExpress.XtraBars.BarButtonItem();
            this.biMeeting = new DevExpress.XtraBars.BarButtonItem();
            this.biPrintSubItem = new DevExpress.XtraBars.BarSubItem();
            this.bmiPrintProfile = new DevExpress.XtraBars.BarButtonItem();
            this.bmiPrintSummary = new DevExpress.XtraBars.BarButtonItem();
            this.bmiPrintDirectory = new DevExpress.XtraBars.BarButtonItem();
            this.bmiPrintTaskList = new DevExpress.XtraBars.BarButtonItem();
            this.biTask = new DevExpress.XtraBars.BarButtonItem();
            this.galleryQuickLetters = new DevExpress.XtraBars.RibbonGalleryBarItem();
            this.biShowMap = new DevExpress.XtraBars.BarButtonItem();
            this.biRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup7 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup4 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup5 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup6 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ZipCodeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.StateImageComboBoxEdit = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.CityTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AddressTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.gridControlTasks = new DevExpress.XtraGrid.GridControl();
            this.bindingSourceTasks = new System.Windows.Forms.BindingSource(this.components);
            this.gvTasks = new DevExpress.DevAV.TaskPreviewGridView();
            this.colPriority = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDueDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubject1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompletion = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemProgressBar1 = new DevExpress.XtraEditors.Repository.RepositoryItemProgressBar();
            this.DepartmentImageComboBoxEdit = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.TitleTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.StatusImageComboBoxEdit = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.HireDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.FirstNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.LastNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.PrefixImageComboBoxEdit = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.BirthDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.PhotoPictureEdit = new DevExpress.XtraEditors.PictureEdit();
            this.HomePhoneTextEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.MobilePhoneTextEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.EmailTextEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.SkypeTextEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForFirstName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLastName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPrefix = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDepartment = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForStatus = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForHomePhone = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForMobilePhone = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEmail = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSkype = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPhoto = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForAddress = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCity = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForState = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForZipCode = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForFullName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForHireDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForBirthDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForTitle = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTasks = new DevExpress.XtraLayout.LayoutControlItem();
            this.bindingSourceEvaluations = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.mvvmContext)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.moduleDataLayout)).BeginInit();
            this.moduleDataLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FullNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ZipCodeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StateImageComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CityTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlTasks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceTasks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvTasks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DepartmentImageComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TitleTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusImageComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HireDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HireDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FirstNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LastNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PrefixImageComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BirthDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BirthDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PhotoPictureEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HomePhoneTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MobilePhoneTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmailTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SkypeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFirstName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLastName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPrefix)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDepartment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHomePhone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMobilePhone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSkype)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPhoto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForZipCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFullName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHireDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBirthDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTasks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceEvaluations)).BeginInit();
            this.SuspendLayout();
            // 
            // moduleDataLayout
            // 
            this.moduleDataLayout.AllowCustomization = false;
            this.moduleDataLayout.Controls.Add(this.chartControl1);
            this.moduleDataLayout.Controls.Add(this.FullNameTextEdit);
            this.moduleDataLayout.Controls.Add(this.ZipCodeTextEdit);
            this.moduleDataLayout.Controls.Add(this.StateImageComboBoxEdit);
            this.moduleDataLayout.Controls.Add(this.CityTextEdit);
            this.moduleDataLayout.Controls.Add(this.AddressTextEdit);
            this.moduleDataLayout.Controls.Add(this.gridControlTasks);
            this.moduleDataLayout.Controls.Add(this.DepartmentImageComboBoxEdit);
            this.moduleDataLayout.Controls.Add(this.TitleTextEdit);
            this.moduleDataLayout.Controls.Add(this.StatusImageComboBoxEdit);
            this.moduleDataLayout.Controls.Add(this.HireDateDateEdit);
            this.moduleDataLayout.Controls.Add(this.FirstNameTextEdit);
            this.moduleDataLayout.Controls.Add(this.LastNameTextEdit);
            this.moduleDataLayout.Controls.Add(this.PrefixImageComboBoxEdit);
            this.moduleDataLayout.Controls.Add(this.BirthDateDateEdit);
            this.moduleDataLayout.Controls.Add(this.PhotoPictureEdit);
            this.moduleDataLayout.Controls.Add(this.HomePhoneTextEdit);
            this.moduleDataLayout.Controls.Add(this.MobilePhoneTextEdit);
            this.moduleDataLayout.Controls.Add(this.EmailTextEdit);
            this.moduleDataLayout.Controls.Add(this.SkypeTextEdit);
            this.moduleDataLayout.DataSource = this.bindingSource;
            this.moduleDataLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.moduleDataLayout.Location = new System.Drawing.Point(0, 217);
            this.moduleDataLayout.Margin = new System.Windows.Forms.Padding(4);
            this.moduleDataLayout.MenuManager = this.ribbonControl;
            this.moduleDataLayout.Name = "moduleDataLayout";
            this.moduleDataLayout.Root = this.layoutControlGroup1;
            this.moduleDataLayout.Size = new System.Drawing.Size(1814, 930);
            this.moduleDataLayout.TabIndex = 1;
            this.moduleDataLayout.Text = "moduleDataLayout";
            // 
            // chartControl1
            // 
            this.chartControl1.DataBindings = null;
            xyDiagram1.AxisX.VisibleInPanesSerializable = "-1";
            xyDiagram1.AxisY.Label.TextPattern = "{V:c0}";
            xyDiagram1.AxisY.Title.Text = "VND";
            xyDiagram1.AxisY.VisibleInPanesSerializable = "-1";
            this.chartControl1.Diagram = xyDiagram1;
            this.chartControl1.Legend.Name = "Default Legend";
            this.chartControl1.Location = new System.Drawing.Point(955, 114);
            this.chartControl1.Name = "chartControl1";
            pointSeriesLabel1.TextPattern = "{V:c0}";
            series1.Label = pointSeriesLabel1;
            series1.LabelsVisibility = DevExpress.Utils.DefaultBoolean.True;
            series1.Name = "Revenue";
            series1.Points.AddRange(new DevExpress.XtraCharts.SeriesPoint[] {
            seriesPoint1,
            seriesPoint2,
            seriesPoint3,
            seriesPoint4,
            seriesPoint5});
            series1.View = lineSeriesView1;
            pointSeriesLabel2.TextPattern = "{V:c0}";
            series2.Label = pointSeriesLabel2;
            series2.LabelsVisibility = DevExpress.Utils.DefaultBoolean.True;
            series2.Name = "Highest By Customer";
            series2.Points.AddRange(new DevExpress.XtraCharts.SeriesPoint[] {
            seriesPoint6,
            seriesPoint7,
            seriesPoint8,
            seriesPoint9,
            seriesPoint10});
            series2.View = lineSeriesView2;
            this.chartControl1.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series1,
        series2};
            this.chartControl1.Size = new System.Drawing.Size(800, 322);
            this.chartControl1.TabIndex = 24;
            // 
            // FullNameTextEdit
            // 
            this.FullNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bindingSource, "FullNameBindable", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.FullNameTextEdit.Location = new System.Drawing.Point(138, 82);
            this.FullNameTextEdit.Margin = new System.Windows.Forms.Padding(4);
            this.FullNameTextEdit.MenuManager = this.ribbonControl;
            this.FullNameTextEdit.Name = "FullNameTextEdit";
            this.FullNameTextEdit.Size = new System.Drawing.Size(482, 26);
            this.FullNameTextEdit.StyleController = this.moduleDataLayout;
            this.FullNameTextEdit.TabIndex = 23;
            // 
            // bindingSource
            // 
            this.bindingSource.DataSource = typeof(DevExpress.DevAV.Employee);
            // 
            // ribbonControl
            // 
            this.ribbonControl.ExpandCollapseItem.Id = 0;
            this.ribbonControl.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl.ExpandCollapseItem,
            this.biSave,
            this.biClose,
            this.biSaveAndClose,
            this.biDelete,
            this.biMailMerge,
            this.biMeeting,
            this.biPrintSubItem,
            this.bmiPrintProfile,
            this.bmiPrintSummary,
            this.biTask,
            this.bmiPrintDirectory,
            this.bmiPrintTaskList,
            this.galleryQuickLetters,
            this.biShowMap,
            this.biRefresh});
            this.ribbonControl.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl.Margin = new System.Windows.Forms.Padding(4);
            this.ribbonControl.MaxItemId = 18;
            this.ribbonControl.Name = "ribbonControl";
            this.ribbonControl.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1});
            this.ribbonControl.Size = new System.Drawing.Size(1814, 217);
            // 
            // biSave
            // 
            this.biSave.Caption = "Save";
            this.biSave.Id = 1;
            this.biSave.ImageOptions.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.False;
            this.biSave.ImageOptions.ImageUri.ResourceType = typeof(DevExpress.DevAV.MainForm);
            this.biSave.ImageOptions.ImageUri.Uri = "resource://DevExpress.DevAV.Resources.Save.svg";
            this.biSave.Name = "biSave";
            // 
            // biClose
            // 
            this.biClose.Caption = "Close";
            this.biClose.Id = 2;
            this.biClose.ImageOptions.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.False;
            this.biClose.ImageOptions.ImageUri.ResourceType = typeof(DevExpress.DevAV.MainForm);
            this.biClose.ImageOptions.ImageUri.Uri = "resource://DevExpress.DevAV.Resources.Close.svg";
            this.biClose.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.Escape);
            this.biClose.Name = "biClose";
            // 
            // biSaveAndClose
            // 
            this.biSaveAndClose.Caption = "Save && Close";
            this.biSaveAndClose.Id = 3;
            this.biSaveAndClose.ImageOptions.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.False;
            this.biSaveAndClose.ImageOptions.ImageUri.ResourceType = typeof(DevExpress.DevAV.MainForm);
            this.biSaveAndClose.ImageOptions.ImageUri.Uri = "resource://DevExpress.DevAV.Resources.SaveAndClose.svg";
            this.biSaveAndClose.Name = "biSaveAndClose";
            // 
            // biDelete
            // 
            this.biDelete.Caption = "Delete";
            this.biDelete.Id = 4;
            this.biDelete.ImageOptions.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.False;
            this.biDelete.ImageOptions.ImageUri.ResourceType = typeof(DevExpress.DevAV.MainForm);
            this.biDelete.ImageOptions.ImageUri.Uri = "resource://DevExpress.DevAV.Resources.Delete.svg";
            this.biDelete.Name = "biDelete";
            // 
            // biMailMerge
            // 
            this.biMailMerge.Caption = "Mail Merge";
            this.biMailMerge.Id = 5;
            this.biMailMerge.ImageOptions.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.False;
            this.biMailMerge.ImageOptions.ImageUri.ResourceType = typeof(DevExpress.DevAV.MainForm);
            this.biMailMerge.ImageOptions.ImageUri.Uri = "resource://DevExpress.DevAV.Resources.MailMerge.svg";
            this.biMailMerge.Name = "biMailMerge";
            // 
            // biMeeting
            // 
            this.biMeeting.Caption = "Meeting";
            this.biMeeting.Id = 6;
            this.biMeeting.ImageOptions.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.False;
            this.biMeeting.ImageOptions.ImageUri.ResourceType = typeof(DevExpress.DevAV.MainForm);
            this.biMeeting.ImageOptions.ImageUri.Uri = "resource://DevExpress.DevAV.Resources.Meeting.svg";
            this.biMeeting.Name = "biMeeting";
            // 
            // biPrintSubItem
            // 
            this.biPrintSubItem.Caption = "Print";
            this.biPrintSubItem.Id = 8;
            this.biPrintSubItem.ImageOptions.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.False;
            this.biPrintSubItem.ImageOptions.ImageUri.ResourceType = typeof(DevExpress.DevAV.MainForm);
            this.biPrintSubItem.ImageOptions.ImageUri.Uri = "resource://DevExpress.DevAV.Resources.Print.svg";
            this.biPrintSubItem.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bmiPrintProfile),
            new DevExpress.XtraBars.LinkPersistInfo(this.bmiPrintSummary),
            new DevExpress.XtraBars.LinkPersistInfo(this.bmiPrintDirectory),
            new DevExpress.XtraBars.LinkPersistInfo(this.bmiPrintTaskList)});
            this.biPrintSubItem.Name = "biPrintSubItem";
            this.biPrintSubItem.ShowNavigationHeader = DevExpress.Utils.DefaultBoolean.True;
            // 
            // bmiPrintProfile
            // 
            this.bmiPrintProfile.Caption = "Employee Profile";
            this.bmiPrintProfile.Id = 9;
            this.bmiPrintProfile.ImageOptions.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.False;
            this.bmiPrintProfile.ImageOptions.ImageUri.ResourceType = typeof(DevExpress.DevAV.MainForm);
            this.bmiPrintProfile.ImageOptions.ImageUri.Uri = "resource://DevExpress.DevAV.Resources.EmployeeCard.svg?Size=16x16";
            this.bmiPrintProfile.Name = "bmiPrintProfile";
            // 
            // bmiPrintSummary
            // 
            this.bmiPrintSummary.Caption = "Summary Report";
            this.bmiPrintSummary.Id = 10;
            this.bmiPrintSummary.ImageOptions.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.False;
            this.bmiPrintSummary.ImageOptions.ImageUri.ResourceType = typeof(DevExpress.DevAV.MainForm);
            this.bmiPrintSummary.ImageOptions.ImageUri.Uri = "resource://DevExpress.DevAV.Resources.Summary.svg?Size=16x16";
            this.bmiPrintSummary.Name = "bmiPrintSummary";
            // 
            // bmiPrintDirectory
            // 
            this.bmiPrintDirectory.Caption = "Directory";
            this.bmiPrintDirectory.Id = 12;
            this.bmiPrintDirectory.ImageOptions.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.False;
            this.bmiPrintDirectory.ImageOptions.ImageUri.ResourceType = typeof(DevExpress.DevAV.MainForm);
            this.bmiPrintDirectory.ImageOptions.ImageUri.Uri = "resource://DevExpress.DevAV.Resources.EmployeeDirectory.svg?Size=16x16";
            this.bmiPrintDirectory.Name = "bmiPrintDirectory";
            // 
            // bmiPrintTaskList
            // 
            this.bmiPrintTaskList.Caption = "Task List";
            this.bmiPrintTaskList.Id = 13;
            this.bmiPrintTaskList.ImageOptions.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.False;
            this.bmiPrintTaskList.ImageOptions.ImageUri.ResourceType = typeof(DevExpress.DevAV.MainForm);
            this.bmiPrintTaskList.ImageOptions.ImageUri.Uri = "resource://DevExpress.DevAV.Resources.TaskList.svg?Size=16x16";
            this.bmiPrintTaskList.Name = "bmiPrintTaskList";
            // 
            // biTask
            // 
            this.biTask.Caption = "Task";
            this.biTask.Id = 11;
            this.biTask.ImageOptions.ImageUri.ResourceType = typeof(DevExpress.DevAV.MainForm);
            this.biTask.ImageOptions.ImageUri.Uri = "resource://DevExpress.DevAV.Resources.Task.svg";
            this.biTask.Name = "biTask";
            // 
            // galleryQuickLetters
            // 
            this.galleryQuickLetters.Caption = "ribbonGalleryBarItem1";
            // 
            // 
            // 
            this.galleryQuickLetters.Gallery.ColumnCount = 2;
            this.galleryQuickLetters.Gallery.DrawImageBackground = false;
            galleryItemGroup1.Caption = "Group1";
            galleryItem1.Caption = "Thank You Note";
            galleryItem1.Image = global::DevExpress.DevAV.Properties.Resources.icon_employee_quick_thank_16;
            galleryItem2.Caption = "Employee Award";
            galleryItem2.Image = global::DevExpress.DevAV.Properties.Resources.icon_employee_quick_award_16;
            galleryItem3.Caption = "Service Excellence";
            galleryItem3.Image = global::DevExpress.DevAV.Properties.Resources.icon_employee_quick_excellence_16;
            galleryItem4.Caption = "Probation Notice";
            galleryItem4.Image = global::DevExpress.DevAV.Properties.Resources.icon_employee_quick_probation_notice_16;
            galleryItem5.Caption = "Welcome To DevAV";
            galleryItem5.Image = global::DevExpress.DevAV.Properties.Resources.icon_employee_quick_welcome_16;
            galleryItemGroup1.Items.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItem[] {
            galleryItem1,
            galleryItem2,
            galleryItem3,
            galleryItem4,
            galleryItem5});
            this.galleryQuickLetters.Gallery.Groups.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItemGroup[] {
            galleryItemGroup1});
            this.galleryQuickLetters.Gallery.ItemImageLocation = DevExpress.Utils.Locations.Left;
            skinPaddingEdges1.Bottom = -3;
            skinPaddingEdges1.Top = -3;
            this.galleryQuickLetters.Gallery.ItemImagePadding = skinPaddingEdges1;
            skinPaddingEdges2.Bottom = -1;
            skinPaddingEdges2.Top = -1;
            this.galleryQuickLetters.Gallery.ItemTextPadding = skinPaddingEdges2;
            this.galleryQuickLetters.Gallery.ShowItemText = true;
            this.galleryQuickLetters.Id = 14;
            this.galleryQuickLetters.Name = "galleryQuickLetters";
            // 
            // biShowMap
            // 
            this.biShowMap.Caption = "Map It";
            this.biShowMap.Id = 15;
            this.biShowMap.ImageOptions.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.False;
            this.biShowMap.ImageOptions.ImageUri.ResourceType = typeof(DevExpress.DevAV.MainForm);
            this.biShowMap.ImageOptions.ImageUri.Uri = "resource://DevExpress.DevAV.Resources.Mapit.svg";
            this.biShowMap.Name = "biShowMap";
            // 
            // biRefresh
            // 
            this.biRefresh.Caption = "Reset Changes";
            this.biRefresh.Id = 17;
            this.biRefresh.ImageOptions.Image = global::DevExpress.DevAV.Properties.Resources.icon_reset_changes_16;
            this.biRefresh.ImageOptions.LargeImage = global::DevExpress.DevAV.Properties.Resources.icon_reset_changes_32;
            this.biRefresh.Name = "biRefresh";
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1,
            this.ribbonPageGroup7,
            this.ribbonPageGroup2,
            this.ribbonPageGroup4,
            this.ribbonPageGroup5,
            this.ribbonPageGroup6,
            this.ribbonPageGroup3});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "CONTACT";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.biSave);
            this.ribbonPageGroup1.ItemLinks.Add(this.biSaveAndClose);
            this.ribbonPageGroup1.MergeOrder = 0;
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.ShowCaptionButton = false;
            this.ribbonPageGroup1.Text = "Save";
            // 
            // ribbonPageGroup7
            // 
            this.ribbonPageGroup7.AllowTextClipping = false;
            this.ribbonPageGroup7.ItemLinks.Add(this.biRefresh);
            this.ribbonPageGroup7.MergeOrder = 0;
            this.ribbonPageGroup7.Name = "ribbonPageGroup7";
            this.ribbonPageGroup7.ShowCaptionButton = false;
            this.ribbonPageGroup7.Text = "Edit";
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.AllowTextClipping = false;
            this.ribbonPageGroup2.ItemLinks.Add(this.biDelete);
            this.ribbonPageGroup2.MergeOrder = 0;
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            this.ribbonPageGroup2.ShowCaptionButton = false;
            this.ribbonPageGroup2.Text = "Delete";
            // 
            // ribbonPageGroup4
            // 
            this.ribbonPageGroup4.ItemLinks.Add(this.biPrintSubItem);
            this.ribbonPageGroup4.ItemLinks.Add(this.biMeeting);
            this.ribbonPageGroup4.ItemLinks.Add(this.biTask);
            this.ribbonPageGroup4.ItemLinks.Add(this.biMailMerge);
            this.ribbonPageGroup4.MergeOrder = 0;
            this.ribbonPageGroup4.Name = "ribbonPageGroup4";
            this.ribbonPageGroup4.ShowCaptionButton = false;
            this.ribbonPageGroup4.Text = "Actions";
            // 
            // ribbonPageGroup5
            // 
            this.ribbonPageGroup5.ItemLinks.Add(this.galleryQuickLetters);
            this.ribbonPageGroup5.MergeOrder = 0;
            this.ribbonPageGroup5.Name = "ribbonPageGroup5";
            this.ribbonPageGroup5.ShowCaptionButton = false;
            this.ribbonPageGroup5.Text = "Quick Letters";
            // 
            // ribbonPageGroup6
            // 
            this.ribbonPageGroup6.AllowTextClipping = false;
            this.ribbonPageGroup6.ItemLinks.Add(this.biShowMap);
            this.ribbonPageGroup6.MergeOrder = 0;
            this.ribbonPageGroup6.Name = "ribbonPageGroup6";
            this.ribbonPageGroup6.ShowCaptionButton = false;
            this.ribbonPageGroup6.Text = "View";
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.AllowTextClipping = false;
            this.ribbonPageGroup3.ItemLinks.Add(this.biClose);
            this.ribbonPageGroup3.MergeOrder = 0;
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            this.ribbonPageGroup3.ShowCaptionButton = false;
            this.ribbonPageGroup3.Text = "Close";
            // 
            // ZipCodeTextEdit
            // 
            this.ZipCodeTextEdit.EnterMoveNextControl = true;
            this.ZipCodeTextEdit.Location = new System.Drawing.Point(364, 262);
            this.ZipCodeTextEdit.Margin = new System.Windows.Forms.Padding(4);
            this.ZipCodeTextEdit.MenuManager = this.ribbonControl;
            this.ZipCodeTextEdit.Name = "ZipCodeTextEdit";
            this.ZipCodeTextEdit.Size = new System.Drawing.Size(445, 26);
            this.ZipCodeTextEdit.StyleController = this.moduleDataLayout;
            this.ZipCodeTextEdit.TabIndex = 22;
            // 
            // StateImageComboBoxEdit
            // 
            this.StateImageComboBoxEdit.EnterMoveNextControl = true;
            this.StateImageComboBoxEdit.Location = new System.Drawing.Point(138, 262);
            this.StateImageComboBoxEdit.Margin = new System.Windows.Forms.Padding(4);
            this.StateImageComboBoxEdit.MenuManager = this.ribbonControl;
            this.StateImageComboBoxEdit.Name = "StateImageComboBoxEdit";
            this.StateImageComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.StateImageComboBoxEdit.Properties.DropDownRows = 14;
            this.StateImageComboBoxEdit.Properties.Sorted = true;
            this.StateImageComboBoxEdit.Size = new System.Drawing.Size(153, 26);
            this.StateImageComboBoxEdit.StyleController = this.moduleDataLayout;
            this.StateImageComboBoxEdit.TabIndex = 21;
            // 
            // CityTextEdit
            // 
            this.CityTextEdit.EnterMoveNextControl = true;
            this.CityTextEdit.Location = new System.Drawing.Point(138, 230);
            this.CityTextEdit.Margin = new System.Windows.Forms.Padding(4);
            this.CityTextEdit.MenuManager = this.ribbonControl;
            this.CityTextEdit.Name = "CityTextEdit";
            this.CityTextEdit.Size = new System.Drawing.Size(671, 26);
            this.CityTextEdit.StyleController = this.moduleDataLayout;
            this.CityTextEdit.TabIndex = 20;
            // 
            // AddressTextEdit
            // 
            this.AddressTextEdit.EnterMoveNextControl = true;
            this.AddressTextEdit.Location = new System.Drawing.Point(138, 198);
            this.AddressTextEdit.Margin = new System.Windows.Forms.Padding(4);
            this.AddressTextEdit.MenuManager = this.ribbonControl;
            this.AddressTextEdit.Name = "AddressTextEdit";
            this.AddressTextEdit.Size = new System.Drawing.Size(671, 26);
            this.AddressTextEdit.StyleController = this.moduleDataLayout;
            this.AddressTextEdit.TabIndex = 19;
            // 
            // gridControlTasks
            // 
            this.gridControlTasks.DataBindings.Add(new System.Windows.Forms.Binding("DataSource", this.bindingSource, "AssignedTasks", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.gridControlTasks.DataSource = this.bindingSourceTasks;
            this.gridControlTasks.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(6);
            this.gridControlTasks.Location = new System.Drawing.Point(138, 462);
            this.gridControlTasks.MainView = this.gvTasks;
            this.gridControlTasks.Margin = new System.Windows.Forms.Padding(4);
            this.gridControlTasks.Name = "gridControlTasks";
            this.gridControlTasks.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemProgressBar1});
            this.gridControlTasks.ShowOnlyPredefinedDetails = true;
            this.gridControlTasks.Size = new System.Drawing.Size(1658, 450);
            this.gridControlTasks.TabIndex = 18;
            this.gridControlTasks.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvTasks});
            // 
            // bindingSourceTasks
            // 
            this.bindingSourceTasks.DataSource = typeof(DevExpress.DevAV.EmployeeTask);
            // 
            // gvTasks
            // 
            this.gvTasks.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.Transparent;
            this.gvTasks.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvTasks.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPriority,
            this.colDueDate,
            this.colSubject1,
            this.colDescription,
            this.colCompletion});
            this.gvTasks.GridControl = this.gridControlTasks;
            this.gvTasks.Name = "gvTasks";
            this.gvTasks.OptionsBehavior.FocusLeaveOnTab = true;
            this.gvTasks.PreviewFieldName = "Description";
            this.gvTasks.PreviewIndent = 0;
            // 
            // colPriority
            // 
            this.colPriority.Caption = "PRIORITY";
            this.colPriority.FieldName = "Priority";
            this.colPriority.Name = "colPriority";
            this.colPriority.OptionsColumn.AllowEdit = false;
            this.colPriority.OptionsColumn.AllowFocus = false;
            this.colPriority.Visible = true;
            this.colPriority.VisibleIndex = 0;
            this.colPriority.Width = 65;
            // 
            // colDueDate
            // 
            this.colDueDate.AppearanceCell.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.colDueDate.AppearanceCell.Options.UseFont = true;
            this.colDueDate.Caption = "DUE DATE";
            this.colDueDate.FieldName = "DueDate";
            this.colDueDate.Name = "colDueDate";
            this.colDueDate.OptionsColumn.AllowEdit = false;
            this.colDueDate.OptionsColumn.AllowFocus = false;
            this.colDueDate.Visible = true;
            this.colDueDate.VisibleIndex = 1;
            this.colDueDate.Width = 80;
            // 
            // colSubject1
            // 
            this.colSubject1.Caption = "SUBJECT";
            this.colSubject1.FieldName = "Subject";
            this.colSubject1.Name = "colSubject1";
            this.colSubject1.OptionsColumn.AllowEdit = false;
            this.colSubject1.OptionsColumn.AllowFocus = false;
            this.colSubject1.Visible = true;
            this.colSubject1.VisibleIndex = 2;
            this.colSubject1.Width = 288;
            // 
            // colDescription
            // 
            this.colDescription.Caption = "DESCRIPTION";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 3;
            this.colDescription.Width = 524;
            // 
            // colCompletion
            // 
            this.colCompletion.Caption = "COMPLETION";
            this.colCompletion.ColumnEdit = this.repositoryItemProgressBar1;
            this.colCompletion.FieldName = "Completion";
            this.colCompletion.Name = "colCompletion";
            this.colCompletion.OptionsColumn.AllowEdit = false;
            this.colCompletion.OptionsColumn.AllowFocus = false;
            this.colCompletion.Visible = true;
            this.colCompletion.VisibleIndex = 4;
            this.colCompletion.Width = 122;
            // 
            // repositoryItemProgressBar1
            // 
            this.repositoryItemProgressBar1.Name = "repositoryItemProgressBar1";
            this.repositoryItemProgressBar1.ShowTitle = true;
            // 
            // DepartmentImageComboBoxEdit
            // 
            this.DepartmentImageComboBoxEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bindingSource, "Department", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.DepartmentImageComboBoxEdit.EnterMoveNextControl = true;
            this.DepartmentImageComboBoxEdit.Location = new System.Drawing.Point(955, 18);
            this.DepartmentImageComboBoxEdit.Margin = new System.Windows.Forms.Padding(4);
            this.DepartmentImageComboBoxEdit.MenuManager = this.ribbonControl;
            this.DepartmentImageComboBoxEdit.Name = "DepartmentImageComboBoxEdit";
            this.DepartmentImageComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DepartmentImageComboBoxEdit.Size = new System.Drawing.Size(841, 26);
            this.DepartmentImageComboBoxEdit.StyleController = this.moduleDataLayout;
            this.DepartmentImageComboBoxEdit.TabIndex = 4;
            // 
            // TitleTextEdit
            // 
            this.TitleTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bindingSource, "Title", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.TitleTextEdit.EnterMoveNextControl = true;
            this.TitleTextEdit.Location = new System.Drawing.Point(138, 146);
            this.TitleTextEdit.Margin = new System.Windows.Forms.Padding(4);
            this.TitleTextEdit.MenuManager = this.ribbonControl;
            this.TitleTextEdit.Name = "TitleTextEdit";
            this.TitleTextEdit.Properties.ValidateOnEnterKey = true;
            this.TitleTextEdit.Size = new System.Drawing.Size(312, 26);
            this.TitleTextEdit.StyleController = this.moduleDataLayout;
            this.TitleTextEdit.TabIndex = 5;
            // 
            // StatusImageComboBoxEdit
            // 
            this.StatusImageComboBoxEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bindingSource, "Status", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.StatusImageComboBoxEdit.EnterMoveNextControl = true;
            this.StatusImageComboBoxEdit.Location = new System.Drawing.Point(955, 50);
            this.StatusImageComboBoxEdit.Margin = new System.Windows.Forms.Padding(4);
            this.StatusImageComboBoxEdit.MenuManager = this.ribbonControl;
            this.StatusImageComboBoxEdit.Name = "StatusImageComboBoxEdit";
            this.StatusImageComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.StatusImageComboBoxEdit.Size = new System.Drawing.Size(841, 26);
            this.StatusImageComboBoxEdit.StyleController = this.moduleDataLayout;
            this.StatusImageComboBoxEdit.TabIndex = 6;
            // 
            // HireDateDateEdit
            // 
            this.HireDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bindingSource, "HireDate", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.HireDateDateEdit.EditValue = null;
            this.HireDateDateEdit.EnterMoveNextControl = true;
            this.HireDateDateEdit.Location = new System.Drawing.Point(955, 82);
            this.HireDateDateEdit.Margin = new System.Windows.Forms.Padding(4);
            this.HireDateDateEdit.MenuManager = this.ribbonControl;
            this.HireDateDateEdit.Name = "HireDateDateEdit";
            this.HireDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.HireDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.HireDateDateEdit.Size = new System.Drawing.Size(841, 26);
            this.HireDateDateEdit.StyleController = this.moduleDataLayout;
            this.HireDateDateEdit.TabIndex = 7;
            // 
            // FirstNameTextEdit
            // 
            this.FirstNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bindingSource, "FirstName", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.FirstNameTextEdit.EnterMoveNextControl = true;
            this.FirstNameTextEdit.Location = new System.Drawing.Point(138, 18);
            this.FirstNameTextEdit.Margin = new System.Windows.Forms.Padding(4);
            this.FirstNameTextEdit.MenuManager = this.ribbonControl;
            this.FirstNameTextEdit.Name = "FirstNameTextEdit";
            this.FirstNameTextEdit.Properties.ValidateOnEnterKey = true;
            this.FirstNameTextEdit.Size = new System.Drawing.Size(482, 26);
            this.FirstNameTextEdit.StyleController = this.moduleDataLayout;
            this.FirstNameTextEdit.TabIndex = 8;
            // 
            // LastNameTextEdit
            // 
            this.LastNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bindingSource, "LastName", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.LastNameTextEdit.EnterMoveNextControl = true;
            this.LastNameTextEdit.Location = new System.Drawing.Point(138, 50);
            this.LastNameTextEdit.Margin = new System.Windows.Forms.Padding(4);
            this.LastNameTextEdit.MenuManager = this.ribbonControl;
            this.LastNameTextEdit.Name = "LastNameTextEdit";
            this.LastNameTextEdit.Properties.ValidateOnEnterKey = true;
            this.LastNameTextEdit.Size = new System.Drawing.Size(482, 26);
            this.LastNameTextEdit.StyleController = this.moduleDataLayout;
            this.LastNameTextEdit.TabIndex = 9;
            // 
            // PrefixImageComboBoxEdit
            // 
            this.PrefixImageComboBoxEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bindingSource, "Prefix", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.PrefixImageComboBoxEdit.EnterMoveNextControl = true;
            this.PrefixImageComboBoxEdit.Location = new System.Drawing.Point(501, 146);
            this.PrefixImageComboBoxEdit.Margin = new System.Windows.Forms.Padding(4);
            this.PrefixImageComboBoxEdit.MenuManager = this.ribbonControl;
            this.PrefixImageComboBoxEdit.Name = "PrefixImageComboBoxEdit";
            this.PrefixImageComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.PrefixImageComboBoxEdit.Size = new System.Drawing.Size(119, 26);
            this.PrefixImageComboBoxEdit.StyleController = this.moduleDataLayout;
            this.PrefixImageComboBoxEdit.TabIndex = 11;
            // 
            // BirthDateDateEdit
            // 
            this.BirthDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bindingSource, "BirthDate", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.BirthDateDateEdit.EditValue = null;
            this.BirthDateDateEdit.EnterMoveNextControl = true;
            this.BirthDateDateEdit.Location = new System.Drawing.Point(138, 114);
            this.BirthDateDateEdit.Margin = new System.Windows.Forms.Padding(4);
            this.BirthDateDateEdit.MenuManager = this.ribbonControl;
            this.BirthDateDateEdit.Name = "BirthDateDateEdit";
            this.BirthDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.BirthDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.BirthDateDateEdit.Size = new System.Drawing.Size(482, 26);
            this.BirthDateDateEdit.StyleController = this.moduleDataLayout;
            this.BirthDateDateEdit.TabIndex = 16;
            // 
            // PhotoPictureEdit
            // 
            this.PhotoPictureEdit.Cursor = System.Windows.Forms.Cursors.Default;
            this.PhotoPictureEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bindingSource, "Photo", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.PhotoPictureEdit.Location = new System.Drawing.Point(643, 17);
            this.PhotoPictureEdit.Margin = new System.Windows.Forms.Padding(4);
            this.PhotoPictureEdit.MenuManager = this.ribbonControl;
            this.PhotoPictureEdit.Name = "PhotoPictureEdit";
            this.PhotoPictureEdit.Properties.PictureStoreMode = DevExpress.XtraEditors.Controls.PictureStoreMode.ByteArray;
            this.PhotoPictureEdit.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.PhotoPictureEdit.Properties.ZoomAccelerationFactor = 1D;
            this.PhotoPictureEdit.Size = new System.Drawing.Size(167, 156);
            this.PhotoPictureEdit.StyleController = this.moduleDataLayout;
            this.PhotoPictureEdit.TabIndex = 17;
            // 
            // HomePhoneTextEdit
            // 
            this.HomePhoneTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bindingSource, "HomePhone", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.HomePhoneTextEdit.EnterMoveNextControl = true;
            this.HomePhoneTextEdit.Location = new System.Drawing.Point(138, 314);
            this.HomePhoneTextEdit.Margin = new System.Windows.Forms.Padding(4);
            this.HomePhoneTextEdit.MenuManager = this.ribbonControl;
            this.HomePhoneTextEdit.Name = "HomePhoneTextEdit";
            this.HomePhoneTextEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::DevExpress.DevAV.Properties.Resources.icon_home_phone_16, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true)});
            this.HomePhoneTextEdit.Properties.ValidateOnEnterKey = true;
            this.HomePhoneTextEdit.Size = new System.Drawing.Size(671, 26);
            this.HomePhoneTextEdit.StyleController = this.moduleDataLayout;
            this.HomePhoneTextEdit.TabIndex = 12;
            // 
            // MobilePhoneTextEdit
            // 
            this.MobilePhoneTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bindingSource, "MobilePhone", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.MobilePhoneTextEdit.EnterMoveNextControl = true;
            this.MobilePhoneTextEdit.Location = new System.Drawing.Point(138, 346);
            this.MobilePhoneTextEdit.Margin = new System.Windows.Forms.Padding(4);
            this.MobilePhoneTextEdit.MenuManager = this.ribbonControl;
            this.MobilePhoneTextEdit.Name = "MobilePhoneTextEdit";
            this.MobilePhoneTextEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::DevExpress.DevAV.Properties.Resources.icon_mobile_phone_16, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "", null, null, true)});
            this.MobilePhoneTextEdit.Properties.ValidateOnEnterKey = true;
            this.MobilePhoneTextEdit.Size = new System.Drawing.Size(671, 26);
            this.MobilePhoneTextEdit.StyleController = this.moduleDataLayout;
            this.MobilePhoneTextEdit.TabIndex = 13;
            // 
            // EmailTextEdit
            // 
            this.EmailTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bindingSource, "Email", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.EmailTextEdit.EnterMoveNextControl = true;
            this.EmailTextEdit.Location = new System.Drawing.Point(138, 378);
            this.EmailTextEdit.Margin = new System.Windows.Forms.Padding(4);
            this.EmailTextEdit.MenuManager = this.ribbonControl;
            this.EmailTextEdit.Name = "EmailTextEdit";
            this.EmailTextEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::DevExpress.DevAV.Properties.Resources.icon_email_16, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject3, "", null, null, true)});
            this.EmailTextEdit.Properties.ValidateOnEnterKey = true;
            this.EmailTextEdit.Size = new System.Drawing.Size(671, 26);
            this.EmailTextEdit.StyleController = this.moduleDataLayout;
            this.EmailTextEdit.TabIndex = 14;
            // 
            // SkypeTextEdit
            // 
            this.SkypeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bindingSource, "Skype", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.SkypeTextEdit.EnterMoveNextControl = true;
            this.SkypeTextEdit.Location = new System.Drawing.Point(138, 410);
            this.SkypeTextEdit.Margin = new System.Windows.Forms.Padding(4);
            this.SkypeTextEdit.MenuManager = this.ribbonControl;
            this.SkypeTextEdit.Name = "SkypeTextEdit";
            this.SkypeTextEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::DevExpress.DevAV.Properties.Resources.icon_skype_16, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject4, "", null, null, true)});
            this.SkypeTextEdit.Properties.ValidateOnEnterKey = true;
            this.SkypeTextEdit.Size = new System.Drawing.Size(671, 26);
            this.SkypeTextEdit.StyleController = this.moduleDataLayout;
            this.SkypeTextEdit.TabIndex = 15;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.ItemForTasks});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.OptionsItemText.TextToControlDistance = 25;
            this.layoutControlGroup1.Size = new System.Drawing.Size(1814, 930);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForFirstName,
            this.ItemForLastName,
            this.ItemForPrefix,
            this.ItemForDepartment,
            this.ItemForStatus,
            this.ItemForHomePhone,
            this.ItemForMobilePhone,
            this.ItemForEmail,
            this.ItemForSkype,
            this.ItemForPhoto,
            this.emptySpaceItem1,
            this.ItemForAddress,
            this.ItemForCity,
            this.ItemForState,
            this.ItemForZipCode,
            this.emptySpaceItem2,
            this.ItemForFullName,
            this.ItemForHireDate,
            this.ItemForBirthDate,
            this.emptySpaceItem3,
            this.emptySpaceItem4,
            this.emptySpaceItem5,
            this.ItemForTitle,
            this.layoutControlItem1});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1784, 444);
            // 
            // ItemForFirstName
            // 
            this.ItemForFirstName.Control = this.FirstNameTextEdit;
            this.ItemForFirstName.CustomizationFormText = "First Name";
            this.ItemForFirstName.Location = new System.Drawing.Point(0, 0);
            this.ItemForFirstName.Name = "ItemForFirstName";
            this.ItemForFirstName.Size = new System.Drawing.Size(608, 32);
            this.ItemForFirstName.Text = "First Name";
            this.ItemForFirstName.TextSize = new System.Drawing.Size(95, 19);
            // 
            // ItemForLastName
            // 
            this.ItemForLastName.Control = this.LastNameTextEdit;
            this.ItemForLastName.CustomizationFormText = "Last Name";
            this.ItemForLastName.Location = new System.Drawing.Point(0, 32);
            this.ItemForLastName.Name = "ItemForLastName";
            this.ItemForLastName.Size = new System.Drawing.Size(608, 32);
            this.ItemForLastName.Text = "Last Name";
            this.ItemForLastName.TextSize = new System.Drawing.Size(95, 19);
            // 
            // ItemForPrefix
            // 
            this.ItemForPrefix.Control = this.PrefixImageComboBoxEdit;
            this.ItemForPrefix.CustomizationFormText = "Prefix";
            this.ItemForPrefix.Location = new System.Drawing.Point(438, 128);
            this.ItemForPrefix.Name = "ItemForPrefix";
            this.ItemForPrefix.Size = new System.Drawing.Size(170, 32);
            this.ItemForPrefix.Text = "Prefix";
            this.ItemForPrefix.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.ItemForPrefix.TextSize = new System.Drawing.Size(40, 19);
            this.ItemForPrefix.TextToControlDistance = 5;
            // 
            // ItemForDepartment
            // 
            this.ItemForDepartment.Control = this.DepartmentImageComboBoxEdit;
            this.ItemForDepartment.CustomizationFormText = "Department";
            this.ItemForDepartment.Location = new System.Drawing.Point(817, 0);
            this.ItemForDepartment.Name = "ItemForDepartment";
            this.ItemForDepartment.Size = new System.Drawing.Size(967, 32);
            this.ItemForDepartment.Text = "Department";
            this.ItemForDepartment.TextSize = new System.Drawing.Size(95, 19);
            // 
            // ItemForStatus
            // 
            this.ItemForStatus.Control = this.StatusImageComboBoxEdit;
            this.ItemForStatus.CustomizationFormText = "Status";
            this.ItemForStatus.Location = new System.Drawing.Point(817, 32);
            this.ItemForStatus.Name = "ItemForStatus";
            this.ItemForStatus.Size = new System.Drawing.Size(967, 32);
            this.ItemForStatus.Text = "Status";
            this.ItemForStatus.TextSize = new System.Drawing.Size(95, 19);
            // 
            // ItemForHomePhone
            // 
            this.ItemForHomePhone.Control = this.HomePhoneTextEdit;
            this.ItemForHomePhone.CustomizationFormText = "Home Phone";
            this.ItemForHomePhone.Location = new System.Drawing.Point(0, 296);
            this.ItemForHomePhone.Name = "ItemForHomePhone";
            this.ItemForHomePhone.Size = new System.Drawing.Size(797, 32);
            this.ItemForHomePhone.Text = "Home Phone";
            this.ItemForHomePhone.TextSize = new System.Drawing.Size(95, 19);
            // 
            // ItemForMobilePhone
            // 
            this.ItemForMobilePhone.Control = this.MobilePhoneTextEdit;
            this.ItemForMobilePhone.CustomizationFormText = "Mobile Phone";
            this.ItemForMobilePhone.Location = new System.Drawing.Point(0, 328);
            this.ItemForMobilePhone.Name = "ItemForMobilePhone";
            this.ItemForMobilePhone.Size = new System.Drawing.Size(797, 32);
            this.ItemForMobilePhone.Text = "Mobile Phone";
            this.ItemForMobilePhone.TextSize = new System.Drawing.Size(95, 19);
            // 
            // ItemForEmail
            // 
            this.ItemForEmail.Control = this.EmailTextEdit;
            this.ItemForEmail.CustomizationFormText = "Email";
            this.ItemForEmail.Location = new System.Drawing.Point(0, 360);
            this.ItemForEmail.Name = "ItemForEmail";
            this.ItemForEmail.Size = new System.Drawing.Size(797, 32);
            this.ItemForEmail.Text = "Email";
            this.ItemForEmail.TextSize = new System.Drawing.Size(95, 19);
            // 
            // ItemForSkype
            // 
            this.ItemForSkype.Control = this.SkypeTextEdit;
            this.ItemForSkype.CustomizationFormText = "Skype";
            this.ItemForSkype.Location = new System.Drawing.Point(0, 392);
            this.ItemForSkype.Name = "ItemForSkype";
            this.ItemForSkype.Size = new System.Drawing.Size(797, 32);
            this.ItemForSkype.Text = "Skype";
            this.ItemForSkype.TextSize = new System.Drawing.Size(95, 19);
            // 
            // ItemForPhoto
            // 
            this.ItemForPhoto.Control = this.PhotoPictureEdit;
            this.ItemForPhoto.CustomizationFormText = "Photo";
            this.ItemForPhoto.Location = new System.Drawing.Point(608, 0);
            this.ItemForPhoto.Name = "ItemForPhoto";
            this.ItemForPhoto.Padding = new DevExpress.XtraLayout.Utils.Padding(20, 2, 2, 2);
            this.ItemForPhoto.Size = new System.Drawing.Size(189, 160);
            this.ItemForPhoto.Text = "Photo:";
            this.ItemForPhoto.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForPhoto.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 160);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(0, 20);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(10, 20);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(797, 20);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForAddress
            // 
            this.ItemForAddress.Control = this.AddressTextEdit;
            this.ItemForAddress.CustomizationFormText = "Address";
            this.ItemForAddress.Location = new System.Drawing.Point(0, 180);
            this.ItemForAddress.Name = "ItemForAddress";
            this.ItemForAddress.Size = new System.Drawing.Size(797, 32);
            this.ItemForAddress.Text = "Address";
            this.ItemForAddress.TextSize = new System.Drawing.Size(95, 19);
            // 
            // ItemForCity
            // 
            this.ItemForCity.Control = this.CityTextEdit;
            this.ItemForCity.CustomizationFormText = "City";
            this.ItemForCity.Location = new System.Drawing.Point(0, 212);
            this.ItemForCity.Name = "ItemForCity";
            this.ItemForCity.Size = new System.Drawing.Size(797, 32);
            this.ItemForCity.Text = "City";
            this.ItemForCity.TextSize = new System.Drawing.Size(95, 19);
            // 
            // ItemForState
            // 
            this.ItemForState.Control = this.StateImageComboBoxEdit;
            this.ItemForState.CustomizationFormText = "State";
            this.ItemForState.Location = new System.Drawing.Point(0, 244);
            this.ItemForState.Name = "ItemForState";
            this.ItemForState.Size = new System.Drawing.Size(279, 32);
            this.ItemForState.Text = "State";
            this.ItemForState.TextSize = new System.Drawing.Size(95, 19);
            // 
            // ItemForZipCode
            // 
            this.ItemForZipCode.Control = this.ZipCodeTextEdit;
            this.ItemForZipCode.CustomizationFormText = "ZipCode";
            this.ItemForZipCode.Location = new System.Drawing.Point(279, 244);
            this.ItemForZipCode.Name = "ItemForZipCode";
            this.ItemForZipCode.Size = new System.Drawing.Size(518, 32);
            this.ItemForZipCode.Text = "ZIP code";
            this.ItemForZipCode.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.ItemForZipCode.TextSize = new System.Drawing.Size(62, 19);
            this.ItemForZipCode.TextToControlDistance = 5;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 276);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(0, 20);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(10, 20);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(797, 20);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForFullName
            // 
            this.ItemForFullName.Control = this.FullNameTextEdit;
            this.ItemForFullName.CustomizationFormText = "Full Name";
            this.ItemForFullName.Location = new System.Drawing.Point(0, 64);
            this.ItemForFullName.Name = "ItemForFullName";
            this.ItemForFullName.Size = new System.Drawing.Size(608, 32);
            this.ItemForFullName.Text = "Full Name";
            this.ItemForFullName.TextSize = new System.Drawing.Size(95, 19);
            // 
            // ItemForHireDate
            // 
            this.ItemForHireDate.Control = this.HireDateDateEdit;
            this.ItemForHireDate.CustomizationFormText = "Hire Date";
            this.ItemForHireDate.Location = new System.Drawing.Point(817, 64);
            this.ItemForHireDate.Name = "ItemForHireDate";
            this.ItemForHireDate.Size = new System.Drawing.Size(967, 32);
            this.ItemForHireDate.Text = "Hire Date";
            this.ItemForHireDate.TextSize = new System.Drawing.Size(95, 19);
            // 
            // ItemForBirthDate
            // 
            this.ItemForBirthDate.Control = this.BirthDateDateEdit;
            this.ItemForBirthDate.CustomizationFormText = "Birth Date";
            this.ItemForBirthDate.Location = new System.Drawing.Point(0, 96);
            this.ItemForBirthDate.Name = "ItemForBirthDate";
            this.ItemForBirthDate.Size = new System.Drawing.Size(608, 32);
            this.ItemForBirthDate.Text = "Birth Date";
            this.ItemForBirthDate.TextSize = new System.Drawing.Size(95, 19);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 424);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(0, 20);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(10, 20);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(1784, 20);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(797, 0);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(20, 0);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(20, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(20, 424);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(1743, 96);
            this.emptySpaceItem5.MaxSize = new System.Drawing.Size(0, 20);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(10, 20);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(41, 328);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForTitle
            // 
            this.ItemForTitle.Control = this.TitleTextEdit;
            this.ItemForTitle.CustomizationFormText = "Title";
            this.ItemForTitle.Location = new System.Drawing.Point(0, 128);
            this.ItemForTitle.Name = "ItemForTitle";
            this.ItemForTitle.Size = new System.Drawing.Size(438, 32);
            this.ItemForTitle.Text = "Title";
            this.ItemForTitle.TextSize = new System.Drawing.Size(95, 19);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.chartControl1;
            this.layoutControlItem1.Location = new System.Drawing.Point(817, 96);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(926, 328);
            this.layoutControlItem1.Text = "Chart";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(95, 19);
            // 
            // ItemForTasks
            // 
            this.ItemForTasks.AppearanceItemCaption.Options.UseTextOptions = true;
            this.ItemForTasks.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.ItemForTasks.Control = this.gridControlTasks;
            this.ItemForTasks.CustomizationFormText = "TASKS";
            this.ItemForTasks.Location = new System.Drawing.Point(0, 444);
            this.ItemForTasks.Name = "ItemForTasks";
            this.ItemForTasks.Size = new System.Drawing.Size(1784, 456);
            this.ItemForTasks.Text = "Tasks";
            this.ItemForTasks.TextSize = new System.Drawing.Size(95, 19);
            // 
            // bindingSourceEvaluations
            // 
            this.bindingSourceEvaluations.DataSource = typeof(DevExpress.DevAV.Evaluation);
            // 
            // EmployeeEditView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.moduleDataLayout);
            this.Controls.Add(this.ribbonControl);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "EmployeeEditView";
            this.Size = new System.Drawing.Size(1814, 1147);
            ((System.ComponentModel.ISupportInitialize)(this.mvvmContext)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.moduleDataLayout)).EndInit();
            this.moduleDataLayout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FullNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ZipCodeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StateImageComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CityTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlTasks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceTasks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvTasks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DepartmentImageComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TitleTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusImageComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HireDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HireDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FirstNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LastNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PrefixImageComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BirthDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BirthDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PhotoPictureEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HomePhoneTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MobilePhoneTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmailTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SkypeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFirstName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLastName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPrefix)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDepartment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHomePhone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMobilePhone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSkype)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPhoto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForZipCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFullName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHireDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBirthDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTasks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceEvaluations)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private XtraBars.Ribbon.RibbonControl ribbonControl;
        private XtraBars.BarButtonItem biSave;
        private XtraBars.BarButtonItem biClose;
        private XtraBars.BarButtonItem biSaveAndClose;
        private XtraBars.BarButtonItem biDelete;
        private XtraBars.Ribbon.RibbonPage ribbonPage1;
        private XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
        private XtraDataLayout.DataLayoutControl moduleDataLayout;
        private XtraLayout.LayoutControlGroup layoutControlGroup1;
        private System.Windows.Forms.BindingSource bindingSource;
        private XtraEditors.ImageComboBoxEdit DepartmentImageComboBoxEdit;
        private XtraEditors.TextEdit TitleTextEdit;
        private XtraEditors.ImageComboBoxEdit StatusImageComboBoxEdit;
        private XtraEditors.DateEdit HireDateDateEdit;
        private XtraEditors.TextEdit FirstNameTextEdit;
        private XtraEditors.TextEdit LastNameTextEdit;
        private XtraEditors.ImageComboBoxEdit PrefixImageComboBoxEdit;
        private XtraEditors.DateEdit BirthDateDateEdit;
        private XtraEditors.PictureEdit PhotoPictureEdit;
        private XtraLayout.LayoutControlGroup layoutControlGroup2;
        private XtraLayout.LayoutControlItem ItemForHomePhone;
        private XtraLayout.LayoutControlItem ItemForMobilePhone;
        private XtraLayout.LayoutControlItem ItemForEmail;
        private XtraLayout.LayoutControlItem ItemForSkype;
        private XtraLayout.LayoutControlItem ItemForFirstName;
        private XtraLayout.LayoutControlItem ItemForPhoto;
        private XtraLayout.LayoutControlItem ItemForLastName;
        private XtraLayout.LayoutControlItem ItemForPrefix;
        private XtraLayout.LayoutControlItem ItemForTitle;
        private XtraLayout.LayoutControlItem ItemForDepartment;
        private XtraLayout.LayoutControlItem ItemForStatus;
        private XtraLayout.LayoutControlItem ItemForHireDate;
        private XtraLayout.LayoutControlItem ItemForBirthDate;
        private System.Windows.Forms.BindingSource bindingSourceEvaluations;
        private XtraGrid.GridControl gridControlTasks;
        private XtraLayout.LayoutControlItem ItemForTasks;
        private System.Windows.Forms.BindingSource bindingSourceTasks;
        private XtraEditors.TextEdit FullNameTextEdit;
        private XtraEditors.TextEdit ZipCodeTextEdit;
        private XtraEditors.ImageComboBoxEdit StateImageComboBoxEdit;
        private XtraEditors.TextEdit CityTextEdit;
        private XtraEditors.TextEdit AddressTextEdit;
        private XtraLayout.EmptySpaceItem emptySpaceItem1;
        private XtraLayout.LayoutControlItem ItemForAddress;
        private XtraLayout.LayoutControlItem ItemForCity;
        private XtraLayout.LayoutControlItem ItemForState;
        private XtraLayout.LayoutControlItem ItemForZipCode;
        private XtraLayout.EmptySpaceItem emptySpaceItem2;
        private XtraLayout.LayoutControlItem ItemForFullName;
        private XtraEditors.ButtonEdit HomePhoneTextEdit;
        private XtraEditors.ButtonEdit MobilePhoneTextEdit;
        private XtraEditors.ButtonEdit EmailTextEdit;
        private XtraEditors.ButtonEdit SkypeTextEdit;
        private XtraLayout.EmptySpaceItem emptySpaceItem3;
        private XtraLayout.EmptySpaceItem emptySpaceItem4;
        private XtraLayout.EmptySpaceItem emptySpaceItem5;
        private XtraGrid.Columns.GridColumn colDueDate;
        private XtraGrid.Columns.GridColumn colSubject1;
        private TaskPreviewGridView gvTasks;
        private XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup4;
        private XtraBars.BarButtonItem biMailMerge;
        private XtraBars.BarButtonItem biMeeting;
        private XtraBars.BarSubItem biPrintSubItem;
        private XtraBars.BarButtonItem bmiPrintProfile;
        private XtraBars.BarButtonItem bmiPrintSummary;
        private XtraBars.BarButtonItem biTask;
        private XtraBars.BarButtonItem bmiPrintDirectory;
        private XtraBars.BarButtonItem bmiPrintTaskList;
        private XtraBars.RibbonGalleryBarItem galleryQuickLetters;
        private XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup5;
        private XtraBars.BarButtonItem biShowMap;
        private XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup6;
        private XtraBars.BarButtonItem biRefresh;
        private XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup7;
        private XtraGrid.Columns.GridColumn colDescription;
        private XtraGrid.Columns.GridColumn colPriority;
        private XtraGrid.Columns.GridColumn colCompletion;
        private XtraEditors.Repository.RepositoryItemProgressBar repositoryItemProgressBar1;
        private XtraCharts.ChartControl chartControl1;
        private XtraLayout.LayoutControlItem layoutControlItem1;
    }
}
